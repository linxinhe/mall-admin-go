package main

import (
	swaggerFiles "github.com/swaggo/files"
	gs "github.com/swaggo/gin-swagger"
	"mall-admin-go/conf"
	_ "mall-admin-go/docs"
	"mall-admin-go/log"
	"mall-admin-go/routers"
)

// @title mall-admin-go-api
// @version 1.0
// @description mall-admin-go-api
// @license.name Apache 2.0
// @contact.name mall-admin-go-api帮助文档
// @contact.url https://github.com/swaggo/swag/blob/master/README_zh-CN.md
// @host localhost:3002
// @BasePath /
func main() {
	conf.Init()
	log.InitFile("D:\\File\\go-interview\\mall\\mall-admin-go\\logs", "server")
	r := routers.NewRouter()
	r.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler))
	_ = r.Run(conf.HttpPort)
}
