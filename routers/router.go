package routers

import (
	"github.com/gin-gonic/gin"
	api "mall-admin-go/api/v1"
	_ "mall-admin-go/docs"
	"mall-admin-go/middleware"
)

func NewRouter() *gin.Engine {
	r := gin.Default()
	r.Use(middleware.Cors(), middleware.LogMiddleware())
	v1 := r.Group("api/v1")
	{
		v1.POST("login", api.AdminLogin)

		authed := v1.Group("/")
		authed.Use(middleware.JWT())
		//普通用户
		authed.GET("user", api.ListUser)
		authed.GET("user/:id", api.InfoUser)

		//管理员
		authed.GET("admin", api.ListAdmin)

		//	商品
		authed.GET("products", api.ListProducts)
		authed.POST("products", api.AddProduct)

		//	分类
		authed.GET("category", api.ListCategories)
		authed.POST("category", api.AddCategories)

		//	上传图片
		authed.POST("upload", api.UploadImg)

		//	通知
		authed.GET("notification", api.ListNotification)
		authed.POST("notification", api.AddNotification)
		authed.DELETE("notification/:id", api.DeleteNotification)

		//	轮播图
		authed.GET("common/carousels", api.ListCarousel)
	}
	return r
}
