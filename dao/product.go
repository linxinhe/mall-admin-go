package dao

import (
	"context"
	"gorm.io/gorm"
	"mall-admin-go/model"
)

type ProductDao struct {
	*gorm.DB
}

func NewProductDao(ctx context.Context) *ProductDao {
	return &ProductDao{NewDBCliet(ctx)}
}

func (dao *ProductDao) GetProduct(page model.BasePage) (products []*model.Product, total int64, err error) {
	err = dao.DB.Model(&model.Product{}).Count(&total).Error
	if err != nil || total == 0 {
		return nil, 0, err
	}
	err = dao.DB.Model(&model.Product{}).
		Offset((page.PageNum - 1) * (page.PageSize)).Limit(page.PageSize).
		Find(&products).Error
	return
}

func (dao *ProductDao) Create(product *model.Product) (err error) {
	err = dao.DB.Model(&model.Product{}).Create(product).Error
	return
}
