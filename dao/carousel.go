package dao

import (
	"context"
	"gorm.io/gorm"
	"mall-admin-go/model"
)

type CarouselDao struct {
	*gorm.DB
}

func NewCarouselDao(ctx context.Context) *CarouselDao {
	return &CarouselDao{NewDBCliet(ctx)}
}

// 获取全部分类
func (dao *CarouselDao) ListCarouse() (categories []*model.Carousel, err error) {
	err = dao.DB.Model(&model.Carousel{}).Find(&categories).Error
	return categories, err
}
