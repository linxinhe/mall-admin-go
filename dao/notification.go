package dao

import (
	"context"
	"gorm.io/gorm"
	"mall-admin-go/model"
)

type NotificationDao struct {
	*gorm.DB
}

func NewNotificationDao(ctx context.Context) *NotificationDao {
	return &NotificationDao{NewDBCliet(ctx)}
}

func (dao *NotificationDao) Get(conditions map[string]interface{}, page model.BasePage) (notification []*model.Notification, total int64, err error) {
	err = dao.DB.Model(&model.Notification{}).Where(conditions).Count(&total).Error
	if err != nil {
		return nil, 0, err
	}
	err = dao.DB.Model(&model.Notification{}).
		Offset((page.PageNum - 1) * (page.PageSize)).Limit(page.PageSize).
		Where(conditions).
		Find(&notification).Error
	if err != nil {
		return nil, 0, err
	}
	return notification, total, nil
}

func (dao *NotificationDao) Create(notification *model.Notification) (err error) {
	err = dao.DB.Model(&model.Notification{}).Create(notification).Error
	return
}

func (dao *NotificationDao) DeleteById(id string) (err error) {
	err = dao.DB.Model(&model.Notification{}).Where("id=?", id).Delete(&model.Notification{}).Error
	return
}
