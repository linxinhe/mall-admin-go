package dao

import (
	"fmt"
	"mall-admin-go/model"
)

func migration() {
	err := _db.Set("gorm:table_options", "charset=utf8mb4").
		AutoMigrate(
			&model.Admin{})
	if err != nil {
		fmt.Println("Error:", err)
	}
}
