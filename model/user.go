package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	ID             uint
	UserName       string `gorm:"unique"`
	Email          string
	PasswordDigest string `gorm:"type-varchar(255) not null"`
	NickName       string
	Status         string
	Avatar         string
	Money          string
	Mobile         string
	Uuid           string
}
