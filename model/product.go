package model

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	Name          string
	Category      string
	Title         string
	Info          string
	ImgPath       string
	Price         float64
	DiscountPrice float64
	OnSale        bool `json:"default:false"`
	Num           int64
	BossId        uint
	BossName      string
	BossAvatar    string
	Uuid          string
	Total         int64
}
