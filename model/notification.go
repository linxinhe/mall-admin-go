package model

import "gorm.io/gorm"

type Notification struct {
	gorm.Model
	ID      uint
	Title   string `gorm:"typevarchar(255) not null"`
	Content string `gorm:"typevarchar(255)"`
	Img     string
}
