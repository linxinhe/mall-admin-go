package model

import "gorm.io/gorm"

type Admin struct {
	gorm.Model
	UserName       string `gorm:"unique"`
	PasswordDigest string `gorm:"type-varchar(255) not null"`
	Email          string
	FullName       string
	role           string
	Mobile         string
	Uuid           string
	AvatarUrl      string
	Token          string
	Status         int
}
