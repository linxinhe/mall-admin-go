package model

import "gorm.io/gorm"

type Categories struct {
	gorm.Model
	ID       uint   `gorm:"not null"`
	Name     string `gorm:"typevarchar(20) not null"`
	ParentId uint
	Img      string `gorm:"typevarchar(20) not null"`
}
