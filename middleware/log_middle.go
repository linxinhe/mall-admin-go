package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"time"
)

func LogMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		c.Next()

		end := time.Now()
		timeSub := end.Sub(start)
		clientIp := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()

		//BodySzie := c.Writer.Status()

		if raw != "" {
			path = path + "?" + raw
		}

		logrus.Infof("[GIN] %s | %d |%d | %s | %s",
			start.Format("2006-01-02_15-04"),
			statusCode,
			timeSub,
			clientIp,
			method,
			path,
		)
	}
}
