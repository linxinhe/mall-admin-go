package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

// ListUser
// @Summary 所有用户列表
// @Description 所有用户列表
// @Produce json
// @Success 200 {object} string "ok"
// @Router /api/v1/user [get]
func ListUser(c *gin.Context) {
	var userService service.UserService
	if err := c.ShouldBind(&userService); err == nil {
		res := userService.ListUser(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}

func InfoUser(c *gin.Context) {
	var userService service.UserService
	if err := c.ShouldBind(&userService); err == nil {
		res := userService.InfoUser(c.Param("id"), c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
