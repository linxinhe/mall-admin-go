package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

func ListCarousel(c *gin.Context) {
	var categoriesCarousel service.CarouselService
	if err := c.ShouldBind(&categoriesCarousel); err == nil {
		res := categoriesCarousel.List(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
