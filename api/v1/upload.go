package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/service"
	"net/http"
)

func UploadImg(c *gin.Context) {
	uploadService := service.UploadService{}
	file, _ := c.FormFile("file")
	if err := c.ShouldBind(&uploadService); err == nil {
		res := uploadService.UploadImg(file)
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, err.Error())
	}
}
