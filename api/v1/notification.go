package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

func ListNotification(c *gin.Context) {
	var notificationService service.NotificationService
	if err := c.ShouldBind(&notificationService); err == nil {
		res := notificationService.List(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
func AddNotification(c *gin.Context) {
	var notificationService service.NotificationService
	if err := c.ShouldBind(&notificationService); err == nil {
		res := notificationService.Add(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
func DeleteNotification(c *gin.Context) {
	var notificationService service.NotificationService
	//claims, _ := util.ParseToken(c.GetHeader("Authorization"))
	if err := c.ShouldBind(&notificationService); err == nil {
		res := notificationService.Delete(c.Request.Context(), c.Param("id"))
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
