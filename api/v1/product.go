package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

func ListProducts(c *gin.Context) {
	var productService service.ProductService
	if err := c.ShouldBind(&productService); err == nil {
		res := productService.List(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
func AddProduct(c *gin.Context) {
	var productService service.ProductService
	if err := c.ShouldBind(&productService); err == nil {
		res := productService.Add(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
