package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

func ListCategories(c *gin.Context) {
	var categoriesService service.CategoriesService
	if err := c.ShouldBind(&categoriesService); err == nil {
		if categoriesService.ParentId != "" {
			res := categoriesService.Show(c.Request.Context(), categoriesService.ParentId)
			c.JSON(http.StatusOK, res)
			return
		}
		res := categoriesService.List(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
func AddCategories(c *gin.Context) {
	var categoriesService service.CategoriesService
	if err := c.ShouldBind(&categoriesService); err == nil {
		res := categoriesService.Add(c.Request.Context(), categoriesService.ParentId, categoriesService.Title, categoriesService.Img)
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}
