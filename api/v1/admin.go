package v1

import (
	"github.com/gin-gonic/gin"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
	"mall-admin-go/service"
	"net/http"
)

// AdminLogin
// @Summary 注册
// @Description 注册
// @Produce json
// @Param userName query string false "用户名" maxlength(20)
// @Param passWord query string false "密码" maxlength(20)
// @Success 200 {object} string "ok"
// @Router /api/v1/login [post]
func AdminLogin(c *gin.Context) {
	var adminService service.AdminService
	if err := c.ShouldBind(&adminService); err == nil {
		res := adminService.AdminLogin(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}
}

func ListAdmin(c *gin.Context) {
	var adminService service.AdminService
	if err := c.ShouldBind(&adminService); err == nil {
		res := adminService.AdminList(c.Request.Context())
		c.JSON(http.StatusOK, res)
	} else {
		c.JSON(http.StatusBadRequest, serializer.ErrorResponse(e.InvalidParams))
	}

}
