package service

import (
	"context"
	"mall-admin-go/dao"
	"mall-admin-go/model"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
)

type NotificationService struct {
	Id      int64  `form:"id"`
	Title   string `form:"title"`
	Content string `form:"content"`
	ImgPath string `form:"img"`
	model.BasePage
}

func (service *NotificationService) List(ctx context.Context) interface{} {
	notificationtDao := dao.NewNotificationDao(ctx)
	if service.PageNum == 0 || service.PageNum < 1 {
		service.PageNum = 1
	}
	if service.PageSize > 15 {
		service.PageNum = 15
	}
	conditions := make(map[string]interface{})
	notification, total, err := notificationtDao.Get(conditions, service.BasePage)
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.BuildListResponse(serializer.BuildNotifications(notification), uint(total), service.PageNum, service.PageSize)
}
func (service *NotificationService) Add(ctx context.Context) interface{} {
	notificationtDao := dao.NewNotificationDao(ctx)

	notificationt := &model.Notification{
		Title:   service.Title,
		Content: service.Content,
		Img:     service.ImgPath,
	}
	err := notificationtDao.Create(notificationt)
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse("添加成功")
}
func (service *NotificationService) Delete(ctx context.Context, id string) interface{} {
	notificationtDao := dao.NewNotificationDao(ctx)
	err := notificationtDao.DeleteById(id)
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse("删除成功")
}
