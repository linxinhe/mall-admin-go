package service

import (
	"context"
	"mall-admin-go/dao"
	"mall-admin-go/model"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
)

type CarouselService struct {
	model.BasePage
}

// List 获取全部轮播图
func (service *CarouselService) List(ctx context.Context) interface{} {

	if service.PageNum == 0 || service.PageNum < 1 {
		service.PageNum = 1
	}
	if service.PageSize > 15 {
		service.PageNum = 15
	}
	carouselDao := dao.NewCarouselDao(ctx)
	categories, err := carouselDao.ListCarouse()
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse(serializer.BuildListCarousels(categories))
}
