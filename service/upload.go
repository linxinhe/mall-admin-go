package service

import (
	"mall-admin-go/pkg/e"
	"mall-admin-go/pkg/util"
	"mall-admin-go/serializer"
	"mime/multipart"
)

type UploadService struct {
}

func (service *UploadService) UploadImg(fileHeader *multipart.FileHeader) interface{} {
	// 打开上传的文件
	uploadedFile, err := fileHeader.Open()
	if err != nil {
		return serializer.ErrorResponse(e.ErrorOpenFile)
	}
	defer uploadedFile.Close()
	status, url := util.UploadToQiNiu(uploadedFile, fileHeader.Size)
	if status != 200 {
		return serializer.ErrorResponse(e.ErrorUploadFile)
	}

	return serializer.SuccessResponse(url)
}
