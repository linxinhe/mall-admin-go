package service

import (
	"context"
	"mall-admin-go/dao"
	"mall-admin-go/pkg/e"
	"mall-admin-go/serializer"
)

type CategoriesService struct {
	Title    string `form:"title"`
	ParentId string `form:"parent_id"`
	Img      string `form:"img"`
}

func (service *CategoriesService) List(ctx context.Context) interface{} {
	categoriesDao := dao.NewCategoriesDao(ctx)
	categories, err := categoriesDao.ListCategories()
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse(serializer.BuildListCategories(categories))
}

func (service *CategoriesService) Show(ctx context.Context, pid string) interface{} {
	categoriesDao := dao.NewCategoriesDao(ctx)
	categories, err := categoriesDao.GetCategoriesByPid(pid)
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse(serializer.BuildListCategories(categories))
}
func (service *CategoriesService) Add(ctx context.Context, pid, name, img string) interface{} {
	categoriesDao := dao.NewCategoriesDao(ctx)
	err := categoriesDao.Create(pid, name, img)
	if err != nil {
		return serializer.ErrorResponse(e.Error)
	}
	return serializer.SuccessResponse(e.Success)
}
