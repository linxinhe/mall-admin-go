package conf

import (
	"github.com/spf13/viper"
	"mall-admin-go/dao"
	"strings"
)

var (
	AppModel string
	HttpPort string

	DB         string
	DbHost     string
	DbPort     string
	DbUser     string
	DbPassword string
	DbName     string

	RedisDb     string
	RedisAddr   string
	RedisPw     string
	RedisDbName string

	AccessKey   string
	SerectKey   string
	Bucket      string
	QiniuServer string

	ValidEmail string
	SmtpHost   string
	SmtEmail   string
	SmtPass    string

	Host        string
	ProductPath string
	AvatarPath  string

	PasswordKey string
	MoneyKey    string
	JwtSecret   string
)

func Init() {
	v := viper.New()
	v.AddConfigPath("./conf") // 路径
	v.SetConfigName("config") // 名称
	v.SetConfigType("ini")    // 类型

	err := v.ReadInConfig() // 读配置
	if err != nil {
		panic(err)
	}
	LoadServer(v)
	LoadMysql(v)
	LoadRedis(v)
	LoadPhotPath(v)
	LoadQiuiu(v)
	LoadKey(v)
	//	mysql (8)读
	pathRead := strings.Join([]string{DbUser, ":", DbPassword, "@tcp(", DbHost, ":", DbPort, ")/", DbName, "?charset=utf8mb4&parseTime=true"}, "")
	//	mysql (2)写
	pathWrite := strings.Join([]string{DbUser, ":", DbPassword, "@tcp(", DbHost, ":", DbPort, ")/", DbName, "?charset=utf8mb4&parseTime=true"}, "")
	dao.Datebase(pathRead, pathWrite)
}

// 加载Server配置
func LoadServer(v *viper.Viper) {
	AppModel = v.GetString("service.AppMode")
	HttpPort = v.GetString("service.HttpPort")
}

// 加载Mysql配置
func LoadMysql(v *viper.Viper) {
	DB = v.GetString("mysql.DB")
	DbHost = v.GetString("mysql.DbHost")
	DbPort = v.GetString("mysql.DbPort")
	DbUser = v.GetString("mysql.DbUser")
	DbPassword = v.GetString("mysql.DbPassword")
	DbName = v.GetString("mysql.DbName")
}

// 加载redis配置
func LoadRedis(v *viper.Viper) {
	RedisDb = v.GetString("redis.RedisDb")
	RedisAddr = v.GetString("redis.RedisAddr")
	RedisPw = v.GetString("redis.RedisPw")
	RedisDbName = v.GetString("redis.RedisDbName")
}

// 加载email配置
//func LoadEmail(v *viper.Viper) {
//	ValidEmail = v.GetString("mysql.DB")
//	SmtpHost = v.GetString("mysql.DB")
//	SmtEmail = v.GetString("mysql.DB")
//	SmtPass = v.GetString("mysql.DB")
//}

// 加载path配置
func LoadPhotPath(v *viper.Viper) {
	Host = v.GetString("path.Host")
	ProductPath = v.GetString("path.ProductPath")
	AvatarPath = v.GetString("path.AvatarPath")
}

// 加载七牛云配置
func LoadQiuiu(v *viper.Viper) {
	AccessKey = v.GetString("qiniu.AccessKey")
	SerectKey = v.GetString("qiniu.SerectKey")
	Bucket = v.GetString("qiniu.Bucket")
	QiniuServer = v.GetString("qiniu.QiniuServer")
}

// 加载密钥配置
func LoadKey(v *viper.Viper) {
	PasswordKey = v.GetString("key.PasswordKey")
	MoneyKey = v.GetString("key.MoneyKey")
	JwtSecret = v.GetString("key.JwtSecret")
}
