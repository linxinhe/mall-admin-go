package serializer

import "mall-admin-go/model"

type Product struct {
	Id            uint    `json:"id"`
	Name          string  `json:"name"`
	Category      string  `json:"category_id"`
	Title         string  `json:"title"`
	Info          string  `json:"info"`
	ImgPath       string  `json:"img_path"`
	Price         float64 `json:"price"`
	DiscountPrice float64 `json:"discount_price"`
	View          uint    `json:"view"`
	CreatedAt     int64   `json:"created_at"`
	Num           int64   `json:"num"`
	OnSale        bool    `json:"onsale"`
	BossId        uint    `json:"boss_id"`
	BossName      string  `json:"boss_name"`
	BossAvatar    string  `json:"boss_avatar"`
	Favorite      bool    `json:"favorite"`
	Uuid          string  `json:"uuid"`
	Total         int64   `json:"total_num"`
}

func BuildProduct(item model.Product) Product {
	return Product{
		Id:            item.ID,
		Name:          item.Name,
		Category:      item.Category,
		Title:         item.Title,
		Info:          item.Info,
		ImgPath:       item.ImgPath,
		Price:         item.Price,
		DiscountPrice: item.DiscountPrice,
		CreatedAt:     item.CreatedAt.Unix(),
		Num:           item.Num,
		OnSale:        item.OnSale,
		BossId:        item.BossId,
		BossName:      item.BossName,
		BossAvatar:    item.BossAvatar,
		Uuid:          item.Uuid,
		Total:         item.Total,
	}
}
func BuildFavoriteProduct(item model.Product, isBool bool) Product {
	return Product{
		Id:            item.ID,
		Name:          item.Name,
		Category:      item.Category,
		Title:         item.Title,
		Info:          item.Info,
		ImgPath:       item.ImgPath,
		Price:         item.Price,
		DiscountPrice: item.DiscountPrice,
		CreatedAt:     item.CreatedAt.Unix(),
		Num:           item.Num,
		OnSale:        item.OnSale,
		BossId:        item.BossId,
		BossName:      item.BossName,
		BossAvatar:    item.BossAvatar,
		Favorite:      isBool,
	}
}

func BuildProducts(items []*model.Product) (products []Product) {
	for _, item := range items {
		product := BuildProduct(*item)
		products = append(products, product)
	}
	return products
}
