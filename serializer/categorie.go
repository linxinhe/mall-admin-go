package serializer

import "mall-admin-go/model"

type Carousel struct {
	Id  uint   `json:"id"`
	Img string `json:"img_path"`
}

func BuildCarousels(item *model.Carousel) Carousel {
	return Carousel{
		Id:  item.ID,
		Img: item.ImgPath,
	}
}

func BuildListCarousels(items []*model.Carousel) (categories []Carousel) {
	for _, item := range items {
		category := BuildCarousels(item)
		categories = append(categories, category)
	}
	return categories
}
