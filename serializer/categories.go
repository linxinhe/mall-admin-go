package serializer

import "mall-admin-go/model"

type Categories struct {
	Id   uint   `json:"id"`
	Name string `json:"name"`
	Img  string `json:"picture"`
	Pid  uint   `json:"pid"`
}

func BuildCategories(item *model.Categories) Categories {
	return Categories{
		Id:   item.ID,
		Name: item.Name,
		Img:  item.Img,
		Pid:  item.ParentId,
	}
}

func BuildListCategories(items []*model.Categories) (categories []Categories) {
	for _, item := range items {
		category := BuildCategories(item)
		categories = append(categories, category)
	}
	return categories
}
