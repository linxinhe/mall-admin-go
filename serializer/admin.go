package serializer

import (
	"mall-admin-go/model"
	"mall-admin-go/pkg/e"
)

type Admin struct {
	Id        uint   `json:"id"`
	UserName  string `json:"user_name"`
	Mobile    string `json:"mobile"`
	Email     string `json:"email"`
	Avatar    string `json:"avatar"`
	Status    int    `json:"status"`
	Roles     string `json:"roles"`
	CreatedAt int64  `json:"created_at"`
}

type LoginRes struct {
	Code int          `json:"code"`
	Data LoginResData `json:"data"`
	Msg  string       `json:"message"`
}

type Position struct {
	CN string `json:"CN"`
	HK string `json:"HK"`
	US string `json:"US"`
}

type permission struct {
	Id        string   `json:"id"`
	Operation []string `json:"operation"`
}
type role struct {
	Id        string   `json:"id"`
	Operation []string `json:"operation"`
}
type LoginUser struct {
	Address   string   `json:"address"`
	Avatar    string   `json:"avatar"`
	Name      string   `json:"name"`
	Positions Position `json:"position"`
}
type LoginResData struct {
	ExpireAt    string       `json:"expireAt"`
	Permissions []permission `json:"permissions"`
	Roles       []role       `json:"roles"`
	Token       string       `json:"token"`
	User        LoginUser    `json:"user"`
}

func BuildAdmin(admin *model.Admin) *Admin {
	return &Admin{
		Id:        admin.ID,
		UserName:  admin.UserName,
		Mobile:    admin.Mobile,
		Email:     admin.Email,
		Avatar:    admin.AvatarUrl,
		Status:    admin.Status,
		Roles:     "",
		CreatedAt: admin.CreatedAt.Unix(),
	}
}
func BuildAdmins(items []*model.Admin) (admins []*Admin) {
	for _, item := range items {
		admin := BuildAdmin(item)
		admins = append(admins, admin)
	}
	return
}

func AdminLoginResponse(admin *model.Admin, token string) *LoginRes {
	//权限列表
	positions := Position{
		CN: "前端工程师 | 蚂蚁金服-计算服务事业群-VUE平台",
		HK: "前端工程師 | 螞蟻金服-計算服務事業群-VUE平台",
		US: "Front-end engineer | Ant Financial - Computing services business group - VUE platform",
	}

	return &LoginRes{
		Code: e.Success,
		Data: LoginResData{
			ExpireAt:    "2023-08-19T04:45:12.485Z",
			Permissions: nil,
			Roles:       nil,
			Token:       token,
			User: LoginUser{
				Address:   "深圳市",
				Avatar:    "https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png",
				Name:      admin.UserName,
				Positions: positions,
			},
		},
		Msg: "中午好，欢迎回来",
	}
}
