package serializer

import "mall-admin-go/pkg/e"

type Response struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
	Msg  string      `json:"msg"`
}

func ErrorResponse(code int) *Response {
	return &Response{
		Code: code,
		Msg:  e.GetMsg(code),
	}
}

func SuccessResponse(data interface{}) *Response {
	return &Response{
		Code: e.Success,
		Msg:  e.GetMsg(e.Success),
		Data: data,
	}
}

type DataList struct {
	List     interface{} `json:"list"`
	Total    uint        `json:"total"`
	PageNum  int         `json:"page"`
	PageSize int         `json:"pageSize"`
}

func BuildListResponse(items interface{}, total uint, page, pageSize int) Response {
	return Response{
		Code: e.Success,
		Data: DataList{
			List:     items,
			Total:    total,
			PageNum:  page,
			PageSize: pageSize,
		},
		Msg: e.GetMsg(e.Success),
	}
}
