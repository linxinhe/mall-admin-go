package serializer

import (
	"mall-admin-go/model"
	"mall-admin-go/pkg/util"
)

type User struct { //vo view objective
	ID        uint   `json:"id"`
	UserName  string `json:"user_name"`
	NickName  string `json:"nick_name"`
	Type      string `json:"type"`
	Email     string `json:"email"`
	Status    string `json:"status"`
	Avatar    string `json:"avatar"`
	CreatedAt int64  `json:"created_at"`
	Mobile    string `json:"mobile"`
	Uuid      string `json:"uid"`
	Money     string `json:"money"`
}

func BuildUser(user *model.User) *User {
	money, _ := util.DecryptMoney(user.Money)
	return &User{
		ID:        user.ID,
		UserName:  user.UserName,
		NickName:  user.NickName,
		Email:     user.Email,
		Status:    user.Status,
		Avatar:    user.Avatar,
		CreatedAt: user.CreatedAt.Unix(),
		Mobile:    user.Mobile,
		Uuid:      user.Uuid,
		Money:     money,
	}
}

func BuildUsers(items []*model.User) (users []*User) {
	for _, item := range items {
		user := BuildUser(item)
		users = append(users, user)
	}
	return users
}
