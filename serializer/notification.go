package serializer

import (
	"mall-admin-go/model"
)

type Notification struct {
	ID      uint   `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Img     string `json:"img"`
	Created int64  `json:"created"`
}

func BuildNotification(item model.Notification) Notification {
	return Notification{
		ID:      item.ID,
		Title:   item.Title,
		Content: item.Content,
		Img:     item.Img,
		Created: item.CreatedAt.Unix(),
	}
}

func BuildNotifications(items []*model.Notification) (notifications []Notification) {
	for _, item := range items {
		notification := BuildNotification(*item)
		notifications = append(notifications, notification)
	}
	return notifications
}
