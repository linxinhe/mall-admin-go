package util

import "time"

func GetDateDay(date int64) (string, string) {
	searchTime := time.Unix(date, 0)
	// 提取日期部分
	searchDate := searchTime.Format("2006-01-02")
	StartDay := searchDate + " 00:00:00"
	EndDay := searchDate + " 23:59:59"
	return StartDay, EndDay
}
