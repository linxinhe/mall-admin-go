package util

import (
	"github.com/google/uuid"
	"regexp"
)

// 生成一个不带-的uuid
func NewUuid() string {
	uuid := uuid.New()
	// 定义正则表达式模式
	pattern := "-"
	// 编译正则表达式
	reg := regexp.MustCompile(pattern)
	// 将匹配到的部分替换为空字符串
	result := reg.ReplaceAllString(uuid.String(), "")
	return result
}
