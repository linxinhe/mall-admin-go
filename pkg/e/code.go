package e

const (
	Success         = 200
	InvalidParams   = 400
	Error           = 500
	ParamsNOt       = 600
	ErrorUploadFile = 700
	ErrorOpenFile   = 701

	//user模块错误

	ErrorExistUser           = 30001
	ErrorUserPassWordDecrypt = 30002
	//ErrorFailEncryption        = 30002
	ErrorExistUserNotFound     = 30003
	ErrorNotCompare            = 30004
	ErrorAuthToken             = 30005
	ErrorAuthCheckTokenTimeout = 30006
	ErrorUploadFail            = 30007
	ErrorSendMail              = 30008
	ErrorMobileAndEmail        = 30009
	ErrorString                = 30010
	ErrorExistPhone            = 30011
	ErrorValidPassword         = 30012
	ErrorMoney                 = 30013
	ErrorUserNot               = 30014
	ErrorAddressNot            = 30015

	//product模块错误 4xxx1
	ErrorProductImgUpload = 40001

	//	收藏夹错误
	ErrorFavoriteExists = 50001

	//	订单错误
	ErrorOrder    = 60001
	ErrorOrderNum = 60002

	//	支付错误
	ErrorPayOrder        = 70001
	ErrorMoneyNot        = 70002
	ErrorUpdateUserMoney = 70003

	//	购物车
	ErrorCartDelete = 80001
)
