package e

var MsgFlags = map[int]string{
	Success:         "ok",
	Error:           "fail",
	InvalidParams:   "参数错误",
	ParamsNOt:       "参数不能为空",
	ErrorUploadFile: "上传失败",
	ErrorOpenFile:   "打开文件失败",

	ErrorExistUser:  "用户名已存在",
	ErrorExistPhone: "手机号已存在",
	//ErrorFailEncryption:        "密码加密失败",
	ErrorUserPassWordDecrypt:   "密码解密失败",
	ErrorExistUserNotFound:     "用户不存在",
	ErrorNotCompare:            "密码错误",
	ErrorAuthToken:             "token认证失败",
	ErrorAuthCheckTokenTimeout: "token过期请重新登录",
	ErrorUploadFail:            "图片上传失败",
	ErrorSendMail:              "邮件发送失败",
	ErrorMobileAndEmail:        "手机号或邮箱格式不正确",
	ErrorString:                "不能有空格等特殊字符",
	ErrorValidPassword:         "密码需要包含大小写字母和数字",
	ErrorMoney:                 "加密金额错误",
	ErrorUserNot:               "用户未注册",
	ErrorAddressNot:            "未找到该地址",

	ErrorProductImgUpload: "图片上传错误",

	ErrorFavoriteExists: "收藏夹错误",

	ErrorOrder:    "创建订单失败",
	ErrorOrderNum: "创建订单失败商品数量不足",

	ErrorPayOrder:        "获取订单信息错误",
	ErrorMoneyNot:        "用户余额不足",
	ErrorUpdateUserMoney: "更新用户余额失败",

	ErrorCartDelete: "删除购物车失败",
}

// GetMsg 获取状态码对应的信息
func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if !ok {
		return MsgFlags[Error]
	}
	return msg
}
