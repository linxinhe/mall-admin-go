# mall-admin-go

#### 介绍
{**该项目是基于golang+gin开发的mall商城后台管理系统，用于管理商品、订单、用户等后台功能。}

#### 软件架构
- 后端框架：使用golang进行后端开发。
- 数据库：采用MySQL作为数据库存储。
- 后端接口：基于RESTful API风格进行接口设计。
- 权限管理：访问权限控制框架casbin。
- 日志管理：采用日志系统记录系统运行日志。
- 单元测试：使用go test进行单元测试。
- 其它框架：


#### 功能特点
- 商品管理：包括商品展示、添加、编辑、删除等功能。
- 订单管理：查看订单列表、订单详情、发货等功能。
- 用户管理：用户列表、用户权限管理等功能。
- 统计报表：销售统计、订单统计等数据报表功能。

#### 安装教程
- Go语言环境
- MySQL数据库

1.  克隆项目到本地：

```
git clone https://gitee.com/wu-huiquan/mall-admin-go.git
```

2.  进入项目目录：

```
cd mall-admin-go
```

3.  安装依赖：

```
go mod tidy
```

4.  配置数据库连接：进入conf/conf.ini配置数据库，redis等信息
5.  启动项目

```
go run main.go
```
#### 项目演示
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/login.png)
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/user-user.png)
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/user-admin.png)
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/product-all.png)
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/fenlei.png)
![输入图片说明](https://www.wisz.club/go-mall/amdin-img/lunbotu.png)